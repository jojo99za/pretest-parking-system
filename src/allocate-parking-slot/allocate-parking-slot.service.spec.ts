import { Test, TestingModule } from '@nestjs/testing';
import { AllocateParkingSlotService } from './allocate-parking-slot.service';

describe('AllocateParkingSlotService', () => {
  let service: AllocateParkingSlotService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AllocateParkingSlotService],
    }).compile();

    service = module.get<AllocateParkingSlotService>(AllocateParkingSlotService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

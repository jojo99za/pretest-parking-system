import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
} from '@nestjs/common';
import { MasterParkingService } from './master-parking.service';
import { CreateMasterParkingDto } from './dto/create-master-parking.dto';
import { UpdateMasterParkingDto } from './dto/update-master-parking.dto';

@Controller('master-parking')
export class MasterParkingController {
  constructor(private readonly masterParkingService: MasterParkingService) {}

  @Post()
  @HttpCode(204)
  create(@Body() createMasterParkingDto: CreateMasterParkingDto) {
    return this.masterParkingService.create(createMasterParkingDto);
  }

  @Post('many')
  @HttpCode(204)
  createMany(@Body() createMasterParkingDto: CreateMasterParkingDto[]) {
    return this.masterParkingService.createMany(createMasterParkingDto);
  }

  @Get()
  findAll() {
    return this.masterParkingService.findAll();
  }

  @Get('/status-parking-lot')
  findStatusParkingLotAll() {
    return this.masterParkingService.findStatusParkingLotAll();
  }

  @Get(':master_pariking_id')
  findOne(@Param('master_pariking_id') master_pariking_id: string) {
    return this.masterParkingService.findOne(master_pariking_id);
  }

  @Get('/status-parking-lot/:master_pariking_id')
  findStatusParkingLot(
    @Param('master_pariking_id') master_pariking_id: string,
  ) {
    return this.masterParkingService.findStatusParkingLot(master_pariking_id);
  }

  @Patch(':master_pariking_id')
  update(
    @Param('master_pariking_id') master_pariking_id: string,
    @Body() updateMasterParkingDto: UpdateMasterParkingDto,
  ) {
    return this.masterParkingService.update(
      master_pariking_id,
      updateMasterParkingDto,
    );
  }

  @Delete(':master_pariking_id')
  remove(@Param('master_pariking_id') master_pariking_id: string) {
    return this.masterParkingService.remove(master_pariking_id);
  }
}

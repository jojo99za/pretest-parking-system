import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CarModule } from './car/car.module';
import { ConfigModule } from '@nestjs/config';
import { MasterParkingModule } from './master-parking/master-parking.module';
import { AllocateParkingSlotModule } from './allocate-parking-slot/allocate-parking-slot.module';

@Module({
  imports: [ConfigModule.forRoot(), CarModule, MasterParkingModule, AllocateParkingSlotModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

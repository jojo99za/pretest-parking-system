export enum CarSize {
  small = 'small',
  medium = 'medium',
  large = 'large',
}

import { ApiProperty } from '@nestjs/swagger';

export class CreateCarDto {
  // car_id: string;
  @ApiProperty()
  plate_number: string;
  @ApiProperty()
  car_size: string;
  // allocate_in_time: Date;
  // allocate_out_time: Date;
}

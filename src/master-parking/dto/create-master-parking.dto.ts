import { ApiProperty } from '@nestjs/swagger';

export class CreateMasterParkingDto {
  @ApiProperty()
  floor_name: string;
  @ApiProperty()
  floor_number: string;
  @ApiProperty()
  total_row: number;
  @ApiProperty()
  total_amount_per_row: number;
}

import { Module } from '@nestjs/common';
import { MasterParkingService } from './master-parking.service';
import { MasterParkingController } from './master-parking.controller';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [MasterParkingController],
  providers: [MasterParkingService, PrismaService],
})
export class MasterParkingModule {}

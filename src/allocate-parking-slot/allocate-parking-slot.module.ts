import { Module } from '@nestjs/common';
import { AllocateParkingSlotService } from './allocate-parking-slot.service';
import { AllocateParkingSlotController } from './allocate-parking-slot.controller';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [AllocateParkingSlotController],
  providers: [AllocateParkingSlotService, PrismaService],
})
export class AllocateParkingSlotModule {}

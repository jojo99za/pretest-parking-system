import { Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { CreateMasterParkingDto } from './dto/create-master-parking.dto';
import { UpdateMasterParkingDto } from './dto/update-master-parking.dto';
import { MasterParking } from './entities/master-parking.entity';

@Injectable()
export class MasterParkingService {
  constructor(@Inject(PrismaService) private prismaService: PrismaService) {}

  async create(
    createMasterParkingDto: CreateMasterParkingDto,
  ): Promise<MasterParking> {
    //create data insert into db master_pariking
    const result = await this.prismaService.master_pariking.create({
      data: {
        ...createMasterParkingDto,
        total_slot:
          createMasterParkingDto.total_row *
          createMasterParkingDto.total_amount_per_row,
      },
    });
    return result;
  }

  async createMany(createMasterParkingDto: CreateMasterParkingDto[]) {
    //create data insert into db master_pariking
    const result_list = await this.prismaService.master_pariking.createMany({
      data: [
        ...createMasterParkingDto.map((row) => {
          return {
            ...row,
            total_slot: row.total_row * row.total_amount_per_row,
          };
        }),
      ],
      skipDuplicates: true,
    });
    return result_list;
  }

  async findAll(): Promise<MasterParking[]> {
    const result = await this.prismaService.master_pariking.findMany({
      // include: {
      //   allocate_parking_slot: true,
      // },
      orderBy: { floor_number: 'asc' },
    });
    return result;
  }

  async findOne(master_pariking_id: string): Promise<MasterParking> {
    return await this.prismaService.master_pariking.findUnique({
      where: {
        master_pariking_id,
      },
    });
  }

  async update(
    master_pariking_id: string,
    updateMasterParkingDto: UpdateMasterParkingDto,
  ): Promise<MasterParking> {
    const result = await this.prismaService.master_pariking.update({
      where: {
        master_pariking_id,
      },
      data: {
        ...updateMasterParkingDto,
      },
    });
    return result;
  }

  async remove(master_pariking_id: string): Promise<MasterParking> {
    //deelte data in allocate_parking_slot
    const result_allocate_parking_slot =
      this.prismaService.allocate_parking_slot.deleteMany({
        where: {
          master_pariking_id,
        },
      });

    //deelte master data in master_pariking
    const result = this.prismaService.master_pariking.delete({
      where: {
        master_pariking_id,
      },
    });

    await this.prismaService.$transaction([
      result_allocate_parking_slot,
      result,
    ]);

    return result;
  }

  async findStatusParkingLot(master_pariking_id: string) {
    const result = await this.prismaService.master_pariking.findUnique({
      where: {
        master_pariking_id,
      },
    });
    //create list of slot_number
    const slot_parking_list = Array.from(Array(result.total_slot).keys()).map(
      (num) => {
        let slot_number = `${result.floor_name}${result.floor_number}-${
          num + 1
        }`;
        return slot_number;
      },
    );
    // console.log('slot_parking_list');
    // console.log(slot_parking_list);

    const result_car_reseved = await this.prismaService.car.findMany({
      where: {
        allocate_out_time: null,
        allocate_parking_slot: {
          master_pariking_id,
        },
      },
      include: {
        allocate_parking_slot: true,
      },
      orderBy: {
        allocate_in_time: 'asc',
      },
    });

    //summary slot empty
    let summary_slot = slot_parking_list.map((slot) => {
      let empty = result_car_reseved.find(
        (row) => row.allocate_parking_slot?.slot_number === slot,
      );
      return {
        slot_number: slot,
        empty: empty === undefined ? true : false,
      };
    });

    //summary total_empty
    let empty_slot = summary_slot.filter((row) => row.empty === true).length;
    let reserve_slot = result_car_reseved.length;

    return {
      ...result,
      empty_slot,
      reserve_slot,
      summary_slot,
    };
  }

  async findStatusParkingLotAll() {
    let temp_result = [];
    const result_list = await this.prismaService.master_pariking.findMany();

    for (const result of result_list) {
      //create list of slot_number
      const slot_parking_list = Array.from(Array(result.total_slot).keys()).map(
        (num) => {
          let slot_number = `${result.floor_name}${result.floor_number}-${
            num + 1
          }`;
          return slot_number;
        },
      );
      // console.log('slot_parking_list');
      // console.log(slot_parking_list);

      const result_car_reseved = await this.prismaService.car.findMany({
        where: {
          allocate_out_time: null,
          allocate_parking_slot: {
            master_pariking_id: result.master_pariking_id,
          },
        },
        include: {
          allocate_parking_slot: true,
        },
        orderBy: {
          allocate_in_time: 'asc',
        },
      });

      //summary slot empty
      let summary_slot = slot_parking_list.map((slot) => {
        let empty = result_car_reseved.find(
          (row) => row.allocate_parking_slot?.slot_number === slot,
        );
        return {
          slot_number: slot,
          empty: empty === undefined ? true : false,
        };
      });

      //summary total_empty
      let empty_slot = summary_slot.filter((row) => row.empty === true).length;
      let reserve_slot = result_car_reseved.length;
      temp_result.push({ ...result, empty_slot, reserve_slot, summary_slot });
    }

    return temp_result;
  }
}

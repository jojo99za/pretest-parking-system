import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { AllocateParkingSlotController } from './allocate-parking-slot.controller';
import { AllocateParkingSlotService } from './allocate-parking-slot.service';

describe('AllocateParkingSlotController', () => {
  let controller: AllocateParkingSlotController;
  let prisma: PrismaService;

  const mockupAllowcate = [
    {
      allocate_parking_slot_id: expect.any(String),
      car_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      master_pariking_id: expect.any(String),
      car: {
        car_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
        plate_number: 'ABC-01',
        car_size: 'small',
        allocate_in_time: '2022-04-15T17:59:13.000Z',
        allocate_out_time: null,
      },
      slot_number: 'G1-1',
      status: 'PARKED',
      slot_seq: 1,
    },
  ];
  const mockAllocateParkingSlotService = {
    findAll: jest.fn().mockImplementation(() => mockupAllowcate),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AllocateParkingSlotController],
      providers: [AllocateParkingSlotService, PrismaService],
    })
      .overrideProvider(AllocateParkingSlotService)
      .useValue(mockAllocateParkingSlotService)
      .compile();

    controller = module.get<AllocateParkingSlotController>(
      AllocateParkingSlotController,
    );
    prisma = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be return all record', async () => {
    expect(await controller.findAll()).toEqual(mockupAllowcate);
  });
});

import { Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
// import { CreateAllocateParkingSlotDto } from './dto/create-allocate-parking-slot.dto';
import { UpdateAllocateParkingSlotDto } from './dto/update-allocate-parking-slot.dto';
import { AllocateParkingSlot } from './entities/allocate-parking-slot.entity';

@Injectable()
export class AllocateParkingSlotService {
  constructor(@Inject(PrismaService) private prismaService: PrismaService) {}

  async findAll(
    car_size?: string,
    status?: string,
  ): Promise<AllocateParkingSlot> {
    let query = {};
    if (car_size) {
      query = {
        ...query,
        car: {
          car_size: car_size.toLocaleLowerCase(),
        },
      };
    }

    if (status) {
      query = { ...query, status: status.toLocaleUpperCase() };
    }
    const result = await this.prismaService.allocate_parking_slot.findMany({
      where: { ...query },
      include: {
        car: true,
      },
    });
    return result;
  }

  async findOne(
    allocate_parking_slot_id: string,
  ): Promise<AllocateParkingSlot> {
    const result = await this.prismaService.allocate_parking_slot.findUnique({
      where: {
        allocate_parking_slot_id,
      },
    });
    return result;
  }

  async update(
    allocate_parking_slot_id: string,
    updateAllocateParkingSlotDto: UpdateAllocateParkingSlotDto,
  ) {
    const result = await this.prismaService.allocate_parking_slot.update({
      where: {
        allocate_parking_slot_id,
      },
      data: {
        ...updateAllocateParkingSlotDto,
      },
    });
    return result;
  }

  async remove(allocate_parking_slot_id: string) {
    const result = await this.prismaService.allocate_parking_slot.delete({
      where: {
        allocate_parking_slot_id,
      },
    });
    return result;
  }
}

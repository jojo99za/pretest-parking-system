import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { CarSize } from 'src/car/enum-car-size/car-size';
import { AllocateParkingSlotService } from './allocate-parking-slot.service';
// import { CreateAllocateParkingSlotDto } from './dto/create-allocate-parking-slot.dto';
import { UpdateAllocateParkingSlotDto } from './dto/update-allocate-parking-slot.dto';
import { Status } from './enum-status/status';

@Controller('allocate-parking-slot')
export class AllocateParkingSlotController {
  constructor(
    private readonly allocateParkingSlotService: AllocateParkingSlotService,
  ) {}

  @Get()
  @ApiQuery({ name: 'car_size', enum: CarSize, required: false })
  @ApiQuery({ name: 'status', enum: Status, required: false })
  findAll(
    @Query('car_size') car_size?: string,
    @Query('status') status?: string,
  ) {
    return this.allocateParkingSlotService.findAll(car_size, status);
  }

  @Get(':allocate_parking_slot_id')
  findOne(@Param('allocate_parking_slot_id') allocate_parking_slot_id: string) {
    return this.allocateParkingSlotService.findOne(allocate_parking_slot_id);
  }

  @Patch(':allocate_parking_slot_id')
  update(
    @Param('allocate_parking_slot_id') allocate_parking_slot_id: string,
    @Body() updateAllocateParkingSlotDto: UpdateAllocateParkingSlotDto,
  ) {
    return this.allocateParkingSlotService.update(
      allocate_parking_slot_id,
      updateAllocateParkingSlotDto,
    );
  }

  @Delete(':allocate_parking_slot_id')
  remove(@Param('allocate_parking_slot_id') allocate_parking_slot_id: string) {
    return this.allocateParkingSlotService.remove(allocate_parking_slot_id);
  }
}

export class MasterParking {
  master_pariking_id: string;
  floor_name: string;
  floor_number: string;
  total_row: number;
  total_amount_per_row: number;
  total_slot: number;
}

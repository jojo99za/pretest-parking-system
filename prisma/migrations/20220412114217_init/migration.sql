CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- CreateTable
CREATE TABLE "allocate_parking_slot" (
    "allocate_parking_slot_id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "car_id" UUID NOT NULL,
    "master_pariking_id" UUID NOT NULL,
    "slot_number" VARCHAR NOT NULL,
    "status" VARCHAR DEFAULT E'PARKED',
    "slot_seq" SMALLINT NOT NULL,

    CONSTRAINT "allocate_parking_slot_pk" PRIMARY KEY ("allocate_parking_slot_id")
);

-- CreateTable
CREATE TABLE "car" (
    "car_id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "plate_number" VARCHAR(10) NOT NULL,
    "car_size" VARCHAR NOT NULL,
    "allocate_in_time" TIMESTAMPTZ(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "allocate_out_time" TIMESTAMPTZ(0),

    CONSTRAINT "car_pk" PRIMARY KEY ("car_id")
);

-- CreateTable
CREATE TABLE "master_pariking" (
    "master_pariking_id" UUID NOT NULL DEFAULT uuid_generate_v4(),
    "floor_name" VARCHAR NOT NULL,
    "floor_number" VARCHAR NOT NULL,
    "total_slot" SMALLINT NOT NULL DEFAULT 0,
    "total_row" SMALLINT NOT NULL DEFAULT 0,
    "total_amount_per_row" SMALLINT NOT NULL DEFAULT 0,

    CONSTRAINT "master_pariking_pk" PRIMARY KEY ("master_pariking_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "allocate_parking_slot_un" ON "allocate_parking_slot"("car_id");

-- AddForeignKey
ALTER TABLE "allocate_parking_slot" ADD CONSTRAINT "allocate_parking_slot_fk" FOREIGN KEY ("car_id") REFERENCES "car"("car_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "allocate_parking_slot" ADD CONSTRAINT "allocate_parking_slot_fk_1" FOREIGN KEY ("master_pariking_id") REFERENCES "master_pariking"("master_pariking_id") ON DELETE NO ACTION ON UPDATE NO ACTION;

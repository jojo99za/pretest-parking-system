import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { MasterParkingModule } from '../src/master-parking/master-parking.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MasterParkingModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  // it('/ (POST) create 1 master-parking', () => {
  //   const mockup = {
  //     floor_name: 'G',
  //     floor_number: '1',
  //     total_row: 2,
  //     total_amount_per_row: 5,
  //   };
  //   return request(app.getHttpServer())
  //     .post('/master-parking')
  //     .send(mockup)
  //     .set('Accept', 'application/json')
  //     .expect(204);
  // });

  // it('/ (POST) create many master-parking', () => {
  //   const mockup = [
  //     {
  //       floor_name: 'G',
  //       floor_number: '1',
  //       total_row: 2,
  //       total_amount_per_row: 5,
  //     },
  //     {
  //       floor_name: 'A',
  //       floor_number: '2',
  //       total_row: 2,
  //       total_amount_per_row: 5,
  //     },
  //   ];
  //   return request(app.getHttpServer())
  //     .post('/master-parking/many')
  //     .send(mockup)
  //     .set('Accept', 'application/json')
  //     .expect(204);
  // });

  // it('/ (POST) park car', () => {
  //   const mockup = {
  //     plate_number: 'ABC-149',
  //     car_size: 'small',
  //   };

  //   return request(app.getHttpServer())
  //     .post('/car/park-car')
  //     .send(mockup)
  //     .set('Accept', 'application/json')
  //     .expect(204);
  // });

  it('/ (POST) park car should be error car_size', () => {
    const mockup = {
      plate_number: 'ABC-149',
      car_size: 'small1',
    };

    return request(app.getHttpServer())
      .post('/car/park-car')
      .send(mockup)
      .set('Accept', 'application/json')
      .expect(500);
  });

  // it('/ (PATCH) leave car parking', () => {
  //   const mockup = {
  //     plate_number: 'ABC-149',
  //     car_size: 'small1',
  //   };

  //   return request(app.getHttpServer())
  //     .patch('/car/leave/4543979b-7b51-48ff-9605-f9df2d819c56') //car_id
  //     .set('Accept', 'application/json')
  //     .expect(200);
  // });

  it('/ (GET) master-parking', () => {
    return request(app.getHttpServer()).get('/master-parking').expect(200);
  });

  it('/ (GET) car', () => {
    return request(app.getHttpServer()).get('/car').expect(200);
  });

  it('/ (GET) car with car_id', () => {
    return request(app.getHttpServer())
      .get('/car/4543979b-7b51-48ff-9605-f9df2d819c56')
      .expect(200);
  });

  it('/ (GET) allocate-parking-slot', () => {
    return request(app.getHttpServer())
      .get('/allocate-parking-slot?car_size=&status=')
      .expect(200);
  });

  it('/ (GET) master-parking findStatusParkingLotAll', () => {
    return request(app.getHttpServer())
      .get('/master-parking/status-parking-lot')
      .expect(200);
  });

  it('/ (GET) master-parking findStatusParkingLot with master-parking-id', () => {
    return request(app.getHttpServer())
      .get(
        '/master-parking/status-parking-lot/9f4e812a-766a-400b-8491-f8179e03f2a0',
      )
      .expect(200);
  });
});

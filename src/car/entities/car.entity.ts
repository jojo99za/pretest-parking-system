export class Car {
  car_id: string;
  plate_number: string;
  car_size: string;
  allocate_in_time: Date;
  allocate_out_time: Date;
}

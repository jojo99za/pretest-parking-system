import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { CreateMasterParkingDto } from './create-master-parking.dto';

export class UpdateMasterParkingDto extends PartialType(
  CreateMasterParkingDto,
) {
  @ApiProperty()
  floor_name: string;
  @ApiProperty()
  floor_number: string;
  @ApiProperty()
  total_row: number;
  @ApiProperty()
  total_amount_per_row: number;
}

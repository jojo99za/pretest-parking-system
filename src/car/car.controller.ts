import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpCode,
} from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';
import { CarService } from './car.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';
import { CarSize } from './enum-car-size/car-size';

@Controller('car')
export class CarController {
  constructor(private readonly carService: CarService) {}

  @Post('park-car')
  @HttpCode(204)
  parkCar(@Body() createCarDto: CreateCarDto) {
    return this.carService.parkCar(createCarDto);
  }

  @Get()
  @ApiQuery({ name: 'car_size', enum: CarSize, required: false })
  findAll(@Query('car_size') car_size?: string) {
    return this.carService.findAll(car_size);
  }

  @Get(':car_id')
  findOne(@Param('car_id') car_id: string) {
    return this.carService.findOne(car_id);
  }

  @Patch(':car_id')
  update(@Param('car_id') car_id: string, @Body() updateCarDto: UpdateCarDto) {
    return this.carService.update(car_id, updateCarDto);
  }

  @Patch('/leave/:car_id')
  leaveSlot(@Param('car_id') car_id: string) {
    return this.carService.leaveSlot(car_id);
  }

  @Delete(':car_id')
  remove(@Param('car_id') car_id: string) {
    return this.carService.remove(car_id);
  }
}

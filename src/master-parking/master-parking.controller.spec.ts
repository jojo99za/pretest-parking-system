import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { CarService } from '../car/car.service';
import { CreateMasterParkingDto } from './dto/create-master-parking.dto';
import { MasterParkingController } from './master-parking.controller';
import { MasterParkingService } from './master-parking.service';

describe('MasterParkingController', () => {
  let controller: MasterParkingController;
  let prisma: PrismaService;

  const mockup: CreateMasterParkingDto = {
    floor_name: 'G',
    floor_number: '1',
    total_row: 2,
    total_amount_per_row: 5,
  };

  const mockUpdata = {
    master_pariking_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
    floor_name: 'A',
    floor_number: '2',
    total_row: 2,
    total_amount_per_row: 5,
    total_slot: 10,
  };

  const mockMasterParkingService = {
    create: jest.fn((dto) => {
      return {
        master_pariking_id: Date.now().toString(),
        total_slot: 10,
        ...dto,
      };
    }),
    update: jest.fn().mockImplementation((master_pariking_id, dto) => ({
      master_pariking_id,
      ...dto,
    })),
    findOne: jest
      .fn()
      .mockImplementation((master_pariking_id) =>
        [mockUpdata].find(
          (row) => row.master_pariking_id === master_pariking_id,
        ),
      ),
    createMany: jest.fn().mockImplementation((dto) => {
      return dto.map((row) => {
        return {
          ...row,
          master_pariking_id: Date.now().toString(),
          total_slot: row.total_row * row.total_amount_per_row,
        };
      });
    }),
    findAll: jest.fn().mockImplementation(() => {
      return [mockUpdata];
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MasterParkingController],
      providers: [MasterParkingService, CarService, PrismaService],
    })
      .overrideProvider(MasterParkingService)
      .useValue(mockMasterParkingService)
      .compile();

    controller = module.get<MasterParkingController>(MasterParkingController);
    prisma = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create master_parking', () => {
    const expectResut = {
      master_pariking_id: expect.any(String),
      floor_name: 'G',
      floor_number: '1',
      total_row: 2,
      total_amount_per_row: 5,
      total_slot: 10,
    };

    expect(controller.create(mockup)).toEqual(expectResut);

    expect(mockMasterParkingService.create).toHaveBeenCalledWith(mockup);
  });

  it('should update master_parking', () => {
    expect(controller.update('1', mockup)).toEqual({
      master_pariking_id: '1',
      ...mockup,
    });

    expect(mockMasterParkingService.update).toHaveBeenCalled();
  });

  it('should findOne master_parking ', async () => {
    //mock service
    prisma.master_pariking.findUnique = jest
      .fn()
      .mockReturnValueOnce(mockUpdata);
    const result = {
      master_pariking_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      floor_name: 'A',
      floor_number: '2',
      total_row: 2,
      total_amount_per_row: 5,
      total_slot: 10,
    };
    expect(
      await mockMasterParkingService.findOne(
        'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      ),
    ).toEqual(result);
  });

  it('should findAll master_parking ', async () => {
    //mock service
    prisma.master_pariking.findMany = jest
      .fn()
      .mockReturnValueOnce([mockUpdata]);
    const result = {
      master_pariking_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      floor_name: 'A',
      floor_number: '2',
      total_row: 2,
      total_amount_per_row: 5,
      total_slot: 10,
    };
    expect(await mockMasterParkingService.findAll()).toEqual([result]);
  });

  it('should createMany master_parking', () => {
    let newMockup = [
      mockup,
      {
        floor_name: 'A',
        floor_number: '2',
        total_row: 2,
        total_amount_per_row: 5,
      },
    ];

    const expectResut = newMockup.map((row) => {
      return {
        ...row,
        master_pariking_id: expect.any(String),
        total_slot: row.total_row * row.total_amount_per_row,
      };
    });

    expect(controller.createMany(newMockup)).toEqual(expectResut);

    expect(mockMasterParkingService.createMany).toHaveBeenCalledWith(newMockup);
  });
});

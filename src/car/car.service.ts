import { Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';
import { Car } from './entities/car.entity';

@Injectable()
export class CarService {
  constructor(@Inject(PrismaService) private prismaService: PrismaService) {}
  async findAll(car_size?: string): Promise<Car[]> {
    return await this.prismaService.car.findMany({
      where: car_size
        ? {
            car_size: car_size.toLowerCase(),
          }
        : {},
    });
  }

  async findOne(car_id: string): Promise<Car> {
    const result = await this.prismaService.car.findUnique({
      where: {
        car_id: car_id,
      },
    });
    if (!result) {
      throw new Error(`Car not found`);
    }
    return result;
  }

  async update(car_id: string, updateCarDto: UpdateCarDto) {
    const result = await this.prismaService.car.update({
      where: {
        car_id,
      },
      data: {
        ...updateCarDto,
      },
    });
    return result;
  }

  async remove(car_id: string) {
    //delete transaction allocate_parking_slot **important delete data relation before
    const result_allocate_parking_slot =
      this.prismaService.allocate_parking_slot.delete({
        where: {
          car_id,
        },
      });

    //delete car
    const result_car = this.prismaService.car.delete({
      where: {
        car_id,
      },
    });

    await this.prismaService.$transaction([
      result_allocate_parking_slot,
      result_car,
    ]);

    return result_car;
  }

  async parkCar(createCarDto: CreateCarDto) {
    //find slot near exit
    //check master_parking floor low -> hight
    //check param car_size
    const car_size = ['small', 'medium', 'large'];
    if (
      !createCarDto.car_size ||
      !car_size.includes(createCarDto.car_size?.toLowerCase())
    ) {
      throw new Error(`Invalid car_size`);
    }
    const result_master_parking =
      await this.prismaService.master_pariking.findMany({
        orderBy: {
          floor_number: 'asc',
        },
      });

    //init field value
    let allocate_item = {
      slot_number: null,
      master_pariking_id: null,
      slot_seq: null,
    };

    //check empty slot near exit (follow master parking)
    for (const floor of result_master_parking) {
      const result_allowcated_count =
        await this.prismaService.allocate_parking_slot.count({
          where: {
            status: 'PARKED',
            master_pariking_id: floor.master_pariking_id,
          },
        });

      //have slot
      if (result_allowcated_count < floor.total_slot) {
        //check near exit slot
        const result_near_exit_slot =
          await this.prismaService.allocate_parking_slot.findFirst({
            where: {
              status: 'LEAVE',
              master_pariking_id: floor.master_pariking_id,
            },
            orderBy: {
              slot_number: 'asc',
            },
          });

        allocate_item.master_pariking_id = floor.master_pariking_id;
        let result_reused = null;
        if (result_near_exit_slot) {
          //recheck have reused
          result_reused =
            await this.prismaService.allocate_parking_slot.findFirst({
              where: {
                status: 'PARKED',
                master_pariking_id: floor.master_pariking_id,
                slot_number: result_near_exit_slot.slot_number,
                slot_seq: result_near_exit_slot.slot_seq,
              },
            });
        }

        if (result_near_exit_slot && !result_reused) {
          //reuse slot
          allocate_item.slot_number = result_near_exit_slot.slot_number;
          allocate_item.slot_seq = result_near_exit_slot.slot_seq;
        } else {
          //generate by {master_pariking.floor_name}{master_pariking.floor_number}-(result_allowcated_count+1)
          allocate_item.slot_number = `${floor.floor_name}${
            floor.floor_number
          }-${result_allowcated_count + 1}`;
          allocate_item.slot_seq = result_allowcated_count + 1;
        }
        break;
      }
    }

    if (allocate_item.master_pariking_id === null) {
      throw new Error(`Slot parking is full.`);
    }
    //insert data into car (for collect log) and reserve slot
    const result_car = await this.prismaService.car.create({
      data: {
        ...createCarDto,
        allocate_parking_slot: {
          create: {
            master_pariking_id: allocate_item?.master_pariking_id,
            slot_number: allocate_item?.slot_number,
            slot_seq: allocate_item.slot_seq,
          },
        },
      },
    });
    return result_car;
  }

  async leaveSlot(car_id: string) {
    const result_car = await this.prismaService.car.findUnique({
      where: {
        car_id,
      },
    });

    if (!result_car) {
      throw new Error(`Not found car.`);
    }

    const result_slot = await this.prismaService.car.update({
      where: {
        car_id,
      },
      data: {
        allocate_out_time: new Date(), //stamp time when leave
        allocate_parking_slot: {
          update: {
            status: 'LEAVE',
          },
        },
      },
    });

    return result_slot;
  }
}

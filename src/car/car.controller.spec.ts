import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '../prisma.service';
import { CarController } from './car.controller';
import { CarService } from './car.service';

describe('CarController', () => {
  let controller: CarController;
  let prisma: PrismaService;

  const mockupCar = [
    {
      car_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      plate_number: 'ABC-03',
      car_size: 'small',
      allocate_in_time: expect.any(Date),
      allocate_out_time: expect.any(Date),
    },
  ];

  const mockCarService = {
    parkCar: jest.fn().mockImplementation((dto) => {
      return {
        ...dto,
        car_id: expect.any(String),
        allocate_in_time: expect.any(Date),
        allocate_out_time: null,
      };
    }),
    leaveSlot: jest.fn().mockImplementation((car_id) => {
      let car = mockupCar.find((row) => row.car_id === car_id);
      return { ...car, allocate_out_time: expect.any(Date) };
    }),
    findAll: jest.fn().mockImplementation(() => mockupCar),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarController],
      providers: [CarService, PrismaService],
    })
      .overrideProvider(CarService)
      .useValue(mockCarService)
      .compile();

    controller = module.get<CarController>(CarController);
    prisma = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be reserved slot', async () => {
    const mockupData = {
      plate_number: 'ABC-01',
      car_size: 'small',
    };

    const expectResut = {
      car_id: expect.any(String),
      plate_number: 'ABC-01',
      car_size: 'small',
      allocate_in_time: expect.any(Date),
      allocate_out_time: null,
    };
    expect(await controller.parkCar(mockupData)).toEqual(expectResut);
  });

  it('should be error with reserved slot', async () => {
    const mockupData = {
      plate_number: 'ABC-01',
      car_size: 'small1',
    };
    try {
      await controller.parkCar(mockupData);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
    }
  });

  it('should be success leave parking', async () => {
    let mockupData = {
      car_id: 'fc6af694-c30b-4d14-a038-c56c2485ffb7',
      plate_number: 'ABC-03',
      car_size: 'small',
    };

    const expectResut = {
      car_id: mockupData.car_id,
      plate_number: 'ABC-03',
      car_size: 'small',
      allocate_in_time: expect.any(Date),
      allocate_out_time: expect.any(Date),
    };

    expect(await controller.leaveSlot(mockupData.car_id)).toEqual(expectResut);
  });

  it('should be success return record filter with car_size', async () => {
    const expectResut = {
      car_id: expect.any(String),
      plate_number: 'ABC-03',
      car_size: 'small',
      allocate_in_time: expect.any(Date),
      allocate_out_time: expect.any(Date),
    };
    expect(await controller.findAll('small')).toEqual([expectResut]);
  });
});

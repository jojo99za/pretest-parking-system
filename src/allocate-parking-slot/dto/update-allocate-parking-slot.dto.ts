import { PartialType } from '@nestjs/mapped-types';
import { CreateAllocateParkingSlotDto } from './create-allocate-parking-slot.dto';

export class UpdateAllocateParkingSlotDto extends PartialType(CreateAllocateParkingSlotDto) {}
